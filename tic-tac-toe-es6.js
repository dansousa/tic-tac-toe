class Game {
  constructor(){
    this.currentPlayer = Game.playerO();
    this.board = new Board();
    this.scoreboard = new Scoreboard();
  }

  playIn(row, coll){
    if(this.board.canPlay() && this.board.canPlayIn(row, coll)){    
      this.board.playIn(this.currentPlayer, row, coll);

      if(this.board.canPlay()){
        if(this.currentPlayer == Game.playerO()){
          this.currentPlayer = Game.playerX();
        }else{
          this.currentPlayer = Game.playerO();
        }

        this.notify(`"${this.currentPlayer}" turn`);

      } else if(this.board.isTie()){
        this.notify("Tie!!!");

      } else{
        this.notify(`"${this.currentPlayer}" won!!!`);
        this.scoreboard.addTo(this.currentPlayer);
      }
    }
  }

  restart(){
    this.board.reset();
    this.scoreboard.reset();
    this.currentPlayer = Game.playerO();
    this.notify(`"${this.currentPlayer}" turn`);
  }

  newMatch(){
    this.board.reset();
    this.currentPlayer = Game.playerO();
    this.notify(`"${this.currentPlayer}" turn`);
  }

  notify(message){
    $('#message').text(message);
  }

  static playerO(){
    return 'O'
  }

  static playerX(){
    return 'X'
  }

}

class Scoreboard {
  constructor(){
    this.score_o = 0;
    this.score_x = 0;
  }

  addTo(player){
    if(player == Game.playerO()){
      this.score_o++;
    }else if(player == Game.playerX()){
      this.score_x++;
    }

    this.print();
  }

  reset(){
    this.score_o = 0;
    this.score_x = 0;

    this.print();
  }

  print(){
    $("#score_o").text(this.score_o);
    $("#score_x").text(this.score_x);
  }
}

class Board {
  constructor(){
    this.cells = [[], [], []];
  }

  playIn(player, row, coll){
    this.cells[row][coll] = player;
    this.print();
  }

  reset(){
    this.cells = [[], [], []];
    $("td").removeClass("win");
    this.print();
  }

  hasWinner(){
    const win_sequences = [
                            [[0, 0], [0, 1], [0, 2]],
                            [[1, 0], [1, 1], [1, 2]],
                            [[2, 0], [2, 1], [2, 2]],
                            [[0, 0], [1, 0], [2, 0]],
                            [[0, 1], [1, 1], [2, 1]],
                            [[0, 2], [1, 2], [2, 2]],
                            [[0, 0], [1, 1], [2, 2]],
                            [[0, 2], [1, 1], [2, 0]]
                          ];

    for(let i in win_sequences){
      if(
          this.cells[win_sequences[i][0][0]][win_sequences[i][0][1]] == this.cells[win_sequences[i][1][0]][win_sequences[i][1][1]] &&
          this.cells[win_sequences[i][0][0]][win_sequences[i][0][1]] == this.cells[win_sequences[i][2][0]][win_sequences[i][2][1]] &&
          this.cells[win_sequences[i][0][0]][win_sequences[i][0][1]] != null
        ){

        $(`.row-${win_sequences[i][0][0]} .cell-${win_sequences[i][0][1]}`).addClass("win");
        $(`.row-${win_sequences[i][1][0]} .cell-${win_sequences[i][1][1]}`).addClass("win");
        $(`.row-${win_sequences[i][2][0]} .cell-${win_sequences[i][2][1]}`).addClass("win");

        return true;
      }
    }

    return false;    
  }

  fullBoard(){
    for (var i = 2; i >= 0; i--) {
      for (var j = 2; j >= 0; j--) {
        if(this.cells[i][j] == null){
          return false;
        }
      }
    }

    return true;
  }

  canPlayIn(row, coll){
    return this.cells[row][coll] == null;
  }

  canPlay(){
    return (!this.hasWinner() && !this.fullBoard());
  }

  isTie(){
    return (!this.canPlay() && !this.hasWinner());
  }

  print(){
    for (let row = 2; row >= 0; row--) {
      for (let coll = 2; coll >= 0; coll--) {
        if(this.cells[row][coll] == Game.playerX()){
          $(`.row-${row} .cell-${coll}`).html('<i class="material-icons dark-gray">clear</i>');

        }else if(this.cells[row][coll] == Game.playerO()){
          $(`.row-${row} .cell-${coll}`).html('<i class="material-icons white">radio_button_unchecked</i>');

        }else{
          $(`.row-${row} .cell-${coll}`).html('');          
        }
      }      
    }
  }
}

const game = new Game();