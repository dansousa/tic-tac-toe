let playerO = 'O';
let playerX = 'X';
let currentPlayer = playerO;

function playIn(row, coll){
  if(board.canPlay() && board.canPlayIn(row, coll)){    
    board.playIn(currentPlayer, row, coll);

    if(board.canPlay()){
      if(currentPlayer == playerO){
        currentPlayer = playerX;
      }else{
        currentPlayer = playerO;
      }

      notify(`"${currentPlayer}" turn`);

    } else if(board.isTie()){
      notify("Tie!!!");

    } else{
      notify(`"${currentPlayer}" won!!!`);
      scoreboard.addTo(currentPlayer);
    }
  }
};

function restart(){
  board.reset();
  scoreboard.reset();
  currentPlayer = playerO;
  notify(`"${currentPlayer}" turn`);
};

function newMatch(){
  board.reset();
  currentPlayer = playerO;
  notify(`"${currentPlayer}" turn`);
};

function notify(message){
  $('#message').text(message);
};

const scoreboard = {
  score_o: 0,
  score_x: 0,

  addTo: function(player){
    if(player == playerO){
      this.score_o++;
    }else if(player == playerX){
      this.score_x++;
    }

    this.print();
  },

  reset: function(){
    this.score_o = 0;
    this.score_x = 0;

    this.print();
  },

  print: function(){
    $("#score_o").text(this.score_o);
    $("#score_x").text(this.score_x);
  }
};

const board = {
  cells: [[], [], []],

  playIn: function(player, row, coll){
    this.cells[row][coll] = player;
    this.print();
  },

  reset: function(){
    this.cells = [[], [], []];
    $("td").removeClass("win");
    this.print();
  },

  hasWinner: function(){
    const win_sequences = [
                            [[0, 0], [0, 1], [0, 2]],
                            [[1, 0], [1, 1], [1, 2]],
                            [[2, 0], [2, 1], [2, 2]],
                            [[0, 0], [1, 0], [2, 0]],
                            [[0, 1], [1, 1], [2, 1]],
                            [[0, 2], [1, 2], [2, 2]],
                            [[0, 0], [1, 1], [2, 2]],
                            [[0, 2], [1, 1], [2, 0]]
                          ];

    for(let i in win_sequences){
      if(
          this.cells[win_sequences[i][0][0]][win_sequences[i][0][1]] == this.cells[win_sequences[i][1][0]][win_sequences[i][1][1]] &&
          this.cells[win_sequences[i][0][0]][win_sequences[i][0][1]] == this.cells[win_sequences[i][2][0]][win_sequences[i][2][1]] &&
          this.cells[win_sequences[i][0][0]][win_sequences[i][0][1]] != null
        ){

        $(`.row-${win_sequences[i][0][0]} .cell-${win_sequences[i][0][1]}`).addClass("win");
        $(`.row-${win_sequences[i][1][0]} .cell-${win_sequences[i][1][1]}`).addClass("win");
        $(`.row-${win_sequences[i][2][0]} .cell-${win_sequences[i][2][1]}`).addClass("win");

        return true;
      }
    }

    return false;    
  },

  fullBoard: function(){
    for (var i = 2; i >= 0; i--) {
      for (var j = 2; j >= 0; j--) {
        if(this.cells[i][j] == null){
          return false;
        }
      }
    }

    return true;
  },

  canPlayIn: function(row, coll){
    return this.cells[row][coll] == null;
  },

  canPlay: function(){
    return (!this.hasWinner() && !this.fullBoard());
  },

  isTie: function(){
    return (!this.canPlay() && !this.hasWinner());
  },

  print: function(){
    for (let row = 2; row >= 0; row--) {
      for (let coll = 2; coll >= 0; coll--) {
        if(this.cells[row][coll] == playerX){
          $(`.row-${row} .cell-${coll}`).html('<i class="material-icons dark-gray">clear</i>');

        }else if(this.cells[row][coll] == playerO){
          $(`.row-${row} .cell-${coll}`).html('<i class="material-icons white">radio_button_unchecked</i>');

        }else{
          $(`.row-${row} .cell-${coll}`).html('');          
        }
      }      
    }
  }
};